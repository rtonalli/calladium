<!-- PROJECT SHIELDS -->
<!--
*** I'm using markdown "reference style" links for readability.
*** Reference links are enclosed in brackets [ ] instead of parentheses ( ).
*** See the bottom of this document for the declaration of the reference variables
*** for contributors-url, forks-url, etc. This is an optional, concise syntax you may use.
*** https://www.markdownguide.org/basic-syntax/#reference-style-links
-->
<!--
[![Contributors][contributors-shield]][contributors-url]
[![Forks][forks-shield]][forks-url]
[![Stargazers][stars-shield]][stars-url]
[![Issues][issues-shield]][issues-url]
[![MIT License][license-shield]][license-url]
[![LinkedIn][linkedin-shield]][linkedin-url]
-->
<!-- PROJECT LOGO -->
<br />
<p text-align="center">
  <a href="https://github.com/othneildrew/Best-README-Template">
    <img src="static/logo.png" alt="Logo" height="100" class="center">
  </a>

  <h1 text-align="center">Calladium</h1>

<p text-align="center">
    <br />
    <br />
    <a href="https://rtonalli.gitlab.io/calladium/">View Site</a>
    ·
    <a href="#contact">Report Bug</a>
    ·
    <a href="#contact">Request Feature</a>
  </p>
</p>

---
<!-- TABLE OF CONTENTS -->
<details open="open">
  <summary>Table of Contents</summary>
  <ol>
    <li>
      <a href="#about-the-project">About the Project</a>
      <ul>
        <li><a href="#built-with">Built With</a></li>
      </ul>
    </li>
    <li>
      <a href="#getting-started">Getting Started</a>
      <ul>
        <li><a href="#prerequisites">Prerequisites</a></li>
        <li><a href="#installation">Installation</a></li>
      </ul>
    </li>
    <li><a href="#usage">Usage</a></li>
    <li><a href="#roadmap">Roadmap</a></li>
    <li><a href="#contributing">Contributing</a></li>
    <li><a href="#license">License</a></li>
    <li><a href="#contact">Contact</a></li>
  </ol>
</details>

---
## About the Project

<img src="static/Screenshot-nuevo.png" alt="screenshot" width="800" class="center">

Calladium is a static website that helps Spanish-speaking non-technical-savvy writers and students to learn how to use LaTeX and Overleaf for their writing projects. 

Currently, many writers are looking for alternatives to write and publish their work, whether it is a book, ebook, research paper, or thesis. However, most tools are paid and oriented to the English-speaking market; some are expensive for individual writers, such as Adobe InDesign.

Also, most of the students and writers in non-technical and non-hard-sciences use traditional word processors that complicate the document layout and design when working with large documents.

This is where LaTeX and Overleaf enter the picture as helpful tools that enable users to self-publish and deliver high-quality, customizable documents.

Calladium is a website built with Hugo and hosted via GitLab Pages. This website contains the resources that guide the intended audience into learning LaTeX and Overleaf.

To generate the website, Hugo takes the source directory of files and templates from Calladium’s GitLab repository and uses them to build the HTML artifact and deploy it on GitLab Pages. The solution follows the documentation as code approach (docs-as-code) to create and maintain documentation.

Calladium also has a React memory game implementation to enable users to practice the most common patterns and commands in LaTeX and Git. The [`brain-memory-muscle-bm2`](https://gitlab.com/santiago.torres/brain-memory-muscle-bm2) GitLab repository hosts and automatically deploys this project to make the memory game URL accessible. This repository also sends a commit to embed the memory game into the Hugo website during the artifact's building and deployment phase.


### Built With

The major frameworks used to build Calladium are:
* [Hugo][hugo]
* [GitLab Pages](https://docs.gitlab.com/ce/user/project/pages/)
    * [GitLab CI][ci]
* [React](https://reactjs.org/)
* [Web Storage API](https://developer.mozilla.org/en-US/docs/Web/API/Web_Storage_API)

<!-- GETTING STARTED -->
## Getting Started

This section describes how to set up a local development environment to collaborate and maintain the Calladium website working locally.

### Prerequisites

You must have installed the following tools to work on the project locally:

- Homebrew
    - Install Homebrew using the following command:

        `/bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"`
- Hugo
    - Install Hugo using the following command:

        `brew install hugo`
  
### Installation

To install this project to work locally, follow the next steps:

1. Clone the repository with HTTPS using the following command:

    `git clone --recursive https://gitlab.com/rtonalli/calladium.git`

    > **NOTE:** Use the `--recursive` flag to clone all of the submodules in the repository, including nested submodules.

2. Preview your project using the following command:

    `hugo server`

    The website runs locally and can be accessed through the URL `http://localhost:1313/calladium/`

3. Start adding content to the website.

    To learn more about how to use Hugo, see the [Getting Started][documentation] documentation.

> **NOTE:** You can optionally build the website by using the command `hugo`, because GitLab is configured to build the website automatically when you commit your changes into the repository. 

<!-- USAGE EXAMPLES -->
## Usage

To add content to the website, you must create new directories under the `/content` directory, and create `_index.md` files and edit them using markdown syntax.

For example, to create a page named **Contacto** with contact information, do the following:

1. Create a new directory with the name `contacto` under the `/content` directory.
2. Create a new file with the name `_index.md` under the `/contacto` directory that you have just created.
3. Edit the `_index.md` file.
    1. Add the name of the page in the title `field` of the front matter section using TOML format. For example:

        ```
        +++
        title = "Contacto"
        weight = 1
        chapter = true
        +++
        ```
    
    2. Add the contact information below the front matter section using markdown syntax.
    3. Save the file to see the changes locally.

> **NOTE:** For more information on how to add content to the website, see [Content Management](https://gohugo.io/content-management/) in the Hugo documentation.

<!-- ROADMAP -->
## Roadmap

For a list of proposed features, see [Issues](https://gitlab.com/rtonalli/calladium/-/issues) in the GitLab repository.

<!-- CONTRIBUTING -->
## Contributing

Any contribution is highly appreciated. To contribute with Calladium, do the following:

1. Fork the project.
2. Create your feature branch using the following command:

    `git checkout -b name_feature_AmazingFeature`

3. Commit your changes using the following command:

    `git commit -m 'Add some AmazingFeature'`

4. Push to the branch using the following command:

    `git push origin feature/AmazingFeature`

5. Open a merge request in GitLab.

<!-- LICENSE -->
## License

Distributed under the Calladium License. 

<!-- CONTACT -->
## Contact

To contact the Calladium team, you can send an email to Libertad Pantoja: <libertad.pantoja@wizeline.com>.

<!-- MARKDOWN LINKS & IMAGES -->
<!-- https://www.markdownguide.org/basic-syntax/#reference-style-links -->
[contributors-shield]: https://img.shields.io/github/contributors/othneildrew/Best-README-Template.svg?style=for-the-badge
[contributors-url]: https://github.com/othneildrew/Best-README-Template/graphs/contributors
[forks-shield]: https://img.shields.io/github/forks/othneildrew/Best-README-Template.svg?style=for-the-badge
[forks-url]: https://github.com/othneildrew/Best-README-Template/network/members
[stars-shield]: https://img.shields.io/github/stars/othneildrew/Best-README-Template.svg?style=for-the-badge
[stars-url]: https://github.com/othneildrew/Best-README-Template/stargazers
[issues-shield]: https://img.shields.io/github/issues/othneildrew/Best-README-Template.svg?style=for-the-badge
[issues-url]: https://github.com/othneildrew/Best-README-Template/issues
[license-shield]: https://img.shields.io/github/license/othneildrew/Best-README-Template.svg?style=for-the-badge
[license-url]: https://github.com/othneildrew/Best-README-Template/blob/master/LICENSE.txt
[linkedin-shield]: https://img.shields.io/badge/-LinkedIn-black.svg?style=for-the-badge&logo=linkedin&colorB=555
[linkedin-url]: https://linkedin.com/in/libertadph/
[product-screenshot]: images/screenshot.png    

[ci]: https://about.gitlab.com/gitlab-ci/
[hugo]: https://gohugo.io
[install]: https://gohugo.io/overview/installing/
[documentation]: https://gohugo.io/overview/introduction/
[userpages]: http://doc.gitlab.com/ee/pages/README.html#user-or-group-pages
[projpages]: http://doc.gitlab.com/ee/pages/README.html#project-pages
[post]: https://about.gitlab.com/2016/04/07/gitlab-pages-setup/#custom-domains
