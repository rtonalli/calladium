+++
title = "Dudas"
chapter = false
weight = 6
+++

## Dudas

### ¿Qué es LaTeX?

Tex es un lenguaje creado para crear documentos en un formato amigable para imprimir, atractivo y consistente. Su principal objetivo es describir cómo se debe ver el documento que estás creando. Tex da muchas opciones para darle a tu documento estructura y formato. LaTex nos provee comandos para usar Tex de una forma sencilla (en sus origenes enfocada en crear fórmulas matemáticas).

### ¿Para qué sirve LaTeX?

LaTeX suele ser usado por la comunidad científica, especialmente la que se dedica a matemáticas o física, para generar documentos como artículos y tesis que requieren un uso consistente de fórmulas matemáticas. Sin embargo, el hecho de poder dar un formato consistente, definir estilos, generar automáticamente tablas de contenido, citas, referencias y figuras, hace de LaTeX una gran herramienta para generar tesis y libros visualmente atractivos.

### ¿Es difícil aprender LaTeX?

Como todo lenguaje, LaTeX tiene una curva de aprendizaje. Es decir, requieres un tiempo para familiarizarte y aprender a usarlo. Uno de los objetivos de Calladium es proveer un ejemplo práctico (escribir un libro de sueños) para familiarizarte con este lenguaje de programación y facilitar el que después lo utilices en cualquier tipo de proyecto que deses.

### ¿Dónde puedo encontrar más información sobre LaTeX?

El [wikibook de LaTeX](https://en.wikibooks.org/wiki/LaTeX/Introduction) es una gran fuente de información (parte del sitio de Calladium está basado en él), sobre todo si te acomoda consultar información en inglés.

Puedes encontrar más información en español en la sección [Quiero aprender LaTeX](/empieza-aqui/aprender-latex/).

Este es un sitio en construcción :) <i class="fas fa-tools"></i>