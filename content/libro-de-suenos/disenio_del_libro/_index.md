+++
title = "¿Cómo cambio el diseño de mi libro?"
weight = 7
chapter = false
+++

## Los comandos generales

### `\documentclass[12pt]{book}`

Para definir el tamaño de la fuente que usarás en tu libro, modifica la información entre corchetes en el comando `\documentclass[12pt]{book}` al inicio de `main.tex`. LaTeX soporta los tamaños 10p, 11pt y 12pt en este comando. Si quieres usar algún otro tamaño deberás establecerlo en cada fragmento de texto usando el comando `\fontsize` como se muestra en la sección Portada.

Otro parámetro que puedes añadir entre corchetes es el tamaño del papel que quieres utilizar. Las opciones de tamaño que soporta LaTeX son:
- a4paper: papel A4 
- letterpaper: tamaño carta 
- a5paper: papel A5
- b5paper: papel B5 
- Legalpaper: tamaño legal
 
Por ejemplo, si quieres cambiar el tamaño del papel a tamaño A4, deberás agregar el parámetro `a4paper` de la siguiente manera:
``` TeX
\documentclass[a4paper]{book}
```
Otra opción que te puede interesar es usar un formato horizontal. Para seleccionar un papel horizontal, agrega el parámetro `landscape` dentro de los corchetes de la siguiente manera:
``` TeX
\documentclass[landscape]{book}
```

Puedes usar más de un parámetro a la vez, por ejemplo el siguiente comando:
``` TeX
\documentclass[a4paper, 10pt, landscape]{book}
```
Indica que quieres usar papel A4, un tamaño de fuente estándar de 10 puntos y que el libro sea horizontal. Con lo cual, tu portada lucirá así:
![landscape](/Landscape.png)

### `\usepackage[utf8]{inputenc}`
El paquete inputenc permite escribir el texto de manera directa y de forma completamente transparente dependiendo de la codificación. Es útil para que LaTeX entienda acentos, letras griegas, entre otros símbolos. 

### `\usepackage{times}`
Este comando establece que queremos usar la fuente de la familia Times (como la Times New Roman). El tipo de fuente que se utiliza por default en LaTeX es computer modern roman, pero hay una variedad de paquetes que pueden utilizarse. Puedes consultar los tipos de letra disponibles en la documentación oficial de [Overleaf](https://www.overleaf.com/learn/latex/Font_typefaces). 

![fonts](/fonts.png)


Donde, “fontpackagename” indica el nombre del paquete que debe colocarse entre llaves. 
Por ejemplo, si quieres utilizar la fuente palatino a lo largo de todo tu documento, utiliza el paquete palatino:
``` TeX
\usepackage{palatino}
```
## Español

Dos de los comandos sugeridos en Overleaf para hacer documentos en español son `\usepackage[T1]{fontenc}` y `\usepackage[spanish]{babel}`. Los puedes encontrar en Calladium bajo el indicador `%%%%%%%%%% Español %%%%%%%%%%`:
``` TeX
%%%%%%%%%% Español %%%%%%%%%%
\usepackage[T1]{fontenc}
\usepackage[spanish]{babel}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
```

### \usepackage[T1]{fontenc}
Contiene codificación de las fuentes específica para el idioma español. Al utilizar este comando evitas que se produzcan errores al usar algunos caracteres.
s
### \usepackage[spanish]{babel}
Permite que los nombres de los elementos del documento, como las secciones, se muestren en español.


### \frontmatter
Da una enumeración romana a los elementos del inicio del libro en lugar de en números arábigos.

## Portada

### Paquetes

Los paquete usados en la sección portada se encuentran en `%%%%%%%%%% Portada %%%%%%%%%%`.

#### \usepackage[textwidth = 16cm]{geometry}

El paquete `geometry` ayuda a establecer el diseño general del espacio para el texto, así como a diseñar los márgenes generales. En este caso el parámetro `textwidth = 16cm` indica que ocuparemos 16 cm como área de texto. 

Esto se aplica de manera general al documento y ya más adelante, específicamente en la portada en el cuerpo del texto (`% ---------- Portada ----------`), utilizamos una nueva geometría para eliminar los márgenes laterales y que la imagen de la portada abarque toda la página a lo ancho:

``` TeX 
\newgeometry{top=1in,bottom=1in,right=0in,left=0in}
```

 Puedes encontrar más información del paquete geometry en [geometry.pdf](https://sites.google.com/site/guiadelatex/4-formato-de-la-pagina-y-escritura-del-texto/04-2-estilos-tamanos-y-margenes-de-pagina-de-pagina/4-2-5-paquete-geometry).

## \usepackage[x11names]{xcolor}

El paquete `xcolor` permite usar una amplia gama de colores, así como formas de codificarlos. Este paquete te permite usar los nombres de 19 colores básicos: black, white, blue, green, yellow, red, entre otros. El parámetro `x11names` permite ingresar a una lista extendida de nombres con más de 300 colores disponibles.
Para utilizar un color escribe el comando `\color`, deberás proporcionarle el nombre del color que quieres usar entre las llaves. Esto hará que tu texto cambie de color. Por ejemplo:

``` TeX 
Voy a visitar a mi tío, el rey de Inglaterra, con mi familia y con mi amigo Humberto. El rey y su familia cercana tienen un perro grande que al nacer fue bautizado. El perro está por morir. Le han puesto sus condecoraciones en casa de mis abuelos para mostrárnoslas.
\\
\color{pink}
El rey es grande, gordo, barbón. Trae puestos varios collares, uno de ellos de cuentas largas y rojas como chiles. A pesar de que el rey es blanco su hijo mayor, mi primo, es de piel negra. Él es mi compañero y tiene una novia igual a él.
```
Causa que el texto a partir del segundo párrafo sea rosa.

![pink](/colorpink.png)


Para definir tu propio color utiliza el comando `\definecolor`. `\definecolor` requiere tres parámetros: `\definecolor{nombre-del-color}{tipo-de-color}{valor(es)}`. Entre las primeras llaves está definido el nombre que le quieres asignar a tu nuevo color, por ejemplo, color de letras. En la segunda llave va el modelo de color que quieres utilizar. Los modelos disponibles son:

- rgb: Red, Green, Blue (rojo,verde,azul). Son tres valores entre cero y uno que definen los componentes de color.
- RGB: el mismo que rgb, pero los valores son números enteros entre 0 y 255.
- cmyk: Cyan, Magenta, Yellow and blacK (cyan, magente, amarillo y negro). Es una lista de 4 números separados por coma entre 0 y 1 (o enteros del 0 al 100) que determinan el color de acuerdo con el mismo modelo que usan la mayor parte de las impresoras.
- gray: Escala de grises. Un número entre 0 y 1.
- HTML: Código del color en hexadecimal sin `#` al inicio.

Por ejemplo: 
`\definecolor{titlepagecolor}{cmyk}{75,68,67,90}`, nos indica un color negro en formato cmyk formado por cierta cantidad de cyan, magenta, amarillo y negro. Mientras que `\definecolor{deeppink}{HTML}{D14064}` Nos indica que estamos usando el color hexadecimal `#D14064`. Puedes consultar las listas de colores y modelos en distintas páginas como en [htmlcolorcodes](https://htmlcolorcodes.com).
