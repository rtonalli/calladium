+++
title = "¿Cómo agrego imágenes?"
weight = 3
chapter = false
+++


1. Para añadir una imagen a tu libro, en la esquina superior izquierda, bajo el Menú de Overleaf, localiza el ícono Subir:
![subir](/subir.png)

2. Al darle click, se desplegará la siguiente pantalla:
![subir](/subirInterno.png)

3. Una vez ahí, selecciona la imagen que quieres agregar a tu libro.
4. Puedes agregar la imagen de tu elección sustituyendo el nombre de tu imagen en `\includegraphics{nombre_de_la_imagen}`, o añadiendo una nueva imagen en este comando. Por ejemplo:
``` Tex
\includegraphics[width=1.5in]{cal.jpeg}
```
