+++
title = "¿Cómo añado una tabla?"
weight = 4
chapter = false
+++


La forma más sencilla de crear una tabla en LaTeX es utilizando un editor externo. En este caso te recomendamos [Tables Generator](https://www.tablesgenerator.com). Esta página te brinda una interfaz muy similar a la que podrías encontrar en una hoja de cálculo tradicional como Excel o Google Spreadsheets.

Una vez creada tu tabla, copia el código, da click en **Generate** y copia el nuevo código a la sección donde quieras agregar la tabla en tu libro.
