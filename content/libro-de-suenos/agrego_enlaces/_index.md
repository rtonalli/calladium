+++
title = "¿Cómo agrego enlaces?"
weight = 5
chapter = false
+++


Para agregar un enlace a un sitio web, busca el comando `\href` o agrega uno nuevo. Dentro de los primeros corchetes verás la dirección del sitio web que se incluye en el enlace y en la segunda el texto con el cual te refieres a él: 

``` Tex
\href{sitio_web}{Nombre del sitio}
```

Por ejemplo: 
``` Tex
\href{https://rtonalli.gitlab.io/calladium/}{Calladium}
```


Este es un sitio en construcción :) <i class="fas fa-tools"></i>

