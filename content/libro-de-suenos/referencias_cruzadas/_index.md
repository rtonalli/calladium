+++
title = "¿Cómo creo referencias cruzadas y etiquetas?"
weight = 6
chapter = false
+++

En LaTeX es fácil referenciar elementos numerados como capítulos, secciones, subsecciones o notas al pie. Para hacerlo hay que definir en la sección, o el elemento numerado que vayas a referenciar una etiqueta (`\label{etiqueta}`), a la que después mandas llamar con el comando `\ref{etiqueta}`.

La etiqueta puede ser solo un nombre para referenciar el elemento, o puede contener algunas letras para identificar lo que se está referenciado como en el caso de fancyref.
Por ejemplo, para referenciar el capítulo Pre-pandemia en el Prólogo:

1. Añade la etiqueta `\label{prepandemia}` inmediatamente después de la definición del capítulo Pre-pandemia. 

```
\chapter{Pre-pandemia}
\label{prepandemia}
```

2. En el Prólogo, o la sección donde decidas añadir la referencia, agrega la etiqueta \ref{prepandemia}.

```
\chapter{Prólogo}
En la Capítulo \ref{prepandemia} Pre-pandemia,...
```
Con lo cual tu referencia se verá así:

![subir](/references.png)
