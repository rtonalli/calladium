+++
title = "¿Cómo cambio de dirección el texto?"
weight = 3
chapter = false
+++

Una forma simple de cambiar la dirección del texto en LaTeX es utilizando el paquete `rotating`. Para usarlo:

1. Establece al inicio de tu archivo `.tex` que vas a utilizar el paquete `rotating` mediante el comando: `\usepackage{rotating}`.
2.  Añade los comandos `\begin{turn}{45}` y `\end{turn}` alrededor  del texto al que le deseas cambiar la dirección. El comando `\begin{turn}{45}` indica, en el segundo juego de llaves, el número de grados que deseas rotar el texto.
    Por ejemplo, los comandos:
    ``` TeX
    \begin{turn}{45} 
    \begin{minipage}{\linewidth}
    Voy a visitar a mi tío, el rey de Inglaterra, con mi familia y con mi amigo Humberto. El rey y su familia cercana tienen un perro grande que al nacer fue bautizado. El perro está por morir. Le han puesto sus condecoraciones en casa de mis abuelos para mostrárnoslas.
    \end{minipage}
    \end{turn}
    ```
    Generan el siguiente texto al ser compilados: 
    ![rotacion](/rotacion.png)

Es importante que uses las etiquetas `\begin{minipage}{\linewidth}` y `\end{minipage}` para evitar que el texto se salga de la página.

