+++
title = "¿Cómo añado una barra de progreso?"
weight = 2
chapter = false
+++

Una de las formas de añadir a tu proyecto una barra de progreso simple es utilizando el paquete `progressbar`. 
Para utilizar el paquete, necesitas invocarlo usando el comando `\usepackage{progressbar}` antes de `\begin{document}`. Para crear una barra de progreso, usa el comando `\progressbar{0.735}`, donde dentro de las llaves indicas el porcentaje de avance como un número entre cero y uno. 

Si utilizas el comando `\progressbar{0.735}`, este te mostrará una pequeña barra donde se ha avanzado el 73.5%. Para ajustar algunas de sus características:

1. Selecciona el ancho de la hoja que tu barra de progreso abarca con el parámetro `width`. Por ejemplo, `\progressbar[width=\textwidth]{0.75}`, indica que tu barra de progreso abarca todo el ancho de la hoja destinado para el texto, mientras que `\progressbar[width=5cm}`indica que quieres una barra que sólo ocupe 5cm del ancho de tu hoja.
2. Indica la altura de la barra de progreso como un porcentaje de la altura del texto con el parámetro `heightr`. Por ejemplo, `\progressbar[width=\textwidth,heightr=1]{0.75}`, quiere decir que tu barra de progreso tiene la misma altura que tu texto. 
    Si necesitas una barra de mayor altura, puedes definir la altura de tu barra con `heighta`. Por ejemplo, `\progressbar[width=\textwidth,heighta=2cm]{0.75}` creará una barra de progreso de 2 cm de espesor.
3. Selecciona los colores de tu barra con los parámetros `filledcolor` y `emptycolor`. Donde, `filledcolor` indica el color de porcentaje que ha avanzado la barra y `emptycolor` el color del resto del espacio. Por ejemplo, el comando:
    ``` TeX
    \progressbar[width=\textwidth,heightr=1,filledcolor=deeppink,emptycolor=lowpink]{0.75}
    ```
    Resulta en la siguiente barra de progreso:
    ![barracolores](/barracolores.png)
4. Cambia el número de subdivisiones en la barra modificando el parámetro `subdivisions`. Por ejemplo, la siguiente barra: 
    ``` TeX
    \progressbar[width=0.9\textwidth,heightr=1,filledcolor=deeppink,emptycolor=lowpink,subdivisions=1]{0.75}
    ```  
    No está graduada ya que está formada por una sola subdivisión.
5. Asigna un valor al parámetro `borderwidth` para modificar el grosor de el perímetro de tu barra de progreso. No es posible llegar a cero, pero el comando:
    ``` TeX
    \progressbar[width=0.9\textwidth,heightr=1,filledcolor=deeppink,emptycolor=lowpink,subdivisions=1, borderwidth=0pt]{0.75}
    ```
    Proporciona el siguiente resultado:
    ![Barrafinal](/Barrafinal.png)


