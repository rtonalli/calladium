+++
title = "Experimentos con texto"
weight = 8
chapter = false
+++

LaTeX cuenta con un gran número de herramientas que pueden dar una mayor libertad de experimentación a los escritores. En LaTeX es posible construir caligramas, líneas de tiempo, barras de progreso (como la usada por Andrea Chapela en el cuento “En proceso” publicado en su libro “Ansibles, perfiladores y otras máquinas de ingenio”.

Los experimentos con texto contenidos en Calladium son:

- [¿Cómo creo una línea del tiempo?](/libro-de-suenos/experimentos_con_texto/linea_del_tiempo/)
- [¿Cómo añado una barra de progreso?](/libro-de-suenos/experimentos_con_texto/barra_de_progreso/)
- [¿Cómo cambio de dirección el texto?](/libro-de-suenos/experimentos_con_texto/direccion_texto/)