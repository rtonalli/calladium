+++
title = "¿Cómo creo una línea del tiempo?"
weight = 1
chapter = false
+++


Hay varias opciones para generar una línea del tiempo. Estas, van desde las muy artesanales donde puedes graficar elemento por elemento de tu línea del tiempo utilizando LaTeX, hasta el uso de paquetes que, provisto que les proporciones algunos parámetros, hacen buena parte del trabajo por ti. 

En este caso exploramos el paquete `chronosys`.

![chronosys](/chronosys.png)

A simple vista, las imágenes que genera podrían parecer muy sencillas, pero chronosys nos brinda la posibilidad de cambiar colores y grosor de la línea, así como la capacidad de mover el texto con respecto a la posición de la línea.

En el ejemplo implementado en Calladium:

``` TeX
\catcode `\@=11
\def\chron@selectmonth#1{\ifcase#1\or enero\or febrero\or
marzo\or abril \or mayo \or junio \or julio \or agosto \or
septiembre \or octubre \or noviembre \or diciembre\fi}
\startchronology
[startyear=2019,stopyear=2022, color=lowpink]
\definechronoperiode{MyPeriod}[color=deeppink, textdepth=0.35cm]
\chronoMyPeriod{2020}{2022}{Pandemia}
\chronoevent{17/11/2019}{Primer caso de COVID documentado}
\chronoevent{1/03/2021}{Inicia la pandemia en méxico}
\stopchronology
```
![linea](/lineadetiempo.png)


1. La primera parte del código (de la línea 1 a la 4) está dedicada a establecer los nombres de los meses en español. Podemos prescindir de ella, con lo cual nuestra línea del tiempo se verá así:
    ![template](/chronosysfrances.png)
    Donde, novembre y mars corresponden a los nombres de los meses en francés.
2. El comando `\stratchronology` indica el inicio de tu línea del tiempo. Los parámetros que recibe, `startyear`, `stopyear` y `color`, se refieren a el año de inicio de la línea del tiempo, el año en el que quieres que termine la línea del tiempo y el color de fondo de la línea, respectivamente.
3. En `\definechronoperiode{MyPeriod}[color=deeppink, textdepth=0.35cm]`, estás definiendo un periodo de tiempo llamado `MyPeriod`, color `deeppink`, donde el texto tendrá una profundidad de 0.35cm con respecto a la línea de tiempo. Esto solo define el periodo de tiempo `MyPeriod` para establecer cómo será, pero no lo crea.
4. Una vez definido `MyPeriod`, podemos crear un periodo de tiempo con las características que ya establecimos con el comando `\chronoMyPeriod{2020}{2022}{Pandemia}`, el cual inicia en año entre las primeras llaves (2020), termina en año entre las segundas llaves (2022) y se llama como estableces en las terceras llaves, en este caso Pandemia.
5. El siguiente comando es `\chronoevent{17/11/2019}{Primer caso de COVID documentado}`, con el cual se establece un evento en particular. En la línea del tiempo, este evento se muestra como una fecha específica en el formato día/mes/año, seguida de la descripción o nombre del evento. Es para este comando que al inicio de la línea del tiempo estableces los nombres de los meses en español. Puedes generar tantos eventos como necesites.
6. Finalmente, para cerrar la línea del tiempo, escribimos `\stopchronology`, esto le indica a `chronosys` que nuestra línea del tiempo terminó.

