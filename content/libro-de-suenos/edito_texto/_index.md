+++
title = "¿Cómo edito el texto?"
weight = 1
chapter = false
+++


Antes de empezar a usar LaTeX, ten en cuenta que un documento sencillo está formado por dos partes esenciales. El inicio, donde el equipo de Calladium definió varios paquetes y comandos para que puedas hacer tu propio libro y el texto en sí,  que está dentro de dos etiquetas: `\begin{document}` y `\end{document}`.

Para facilitar el uso de esta plantilla, cada sección tanto dentro como fuera del cuerpo del documento tiene su título y su final demarcados por comentarios. Los comentarios son textos que puedes ver en el editor de LaTeX en el archivo `main.tex`, pero no aparecerán en tu libro. Su objetivo es brindar indicaciones acerca de para qué están ahí algunos comandos o elementos.
Todas las secciones que puedes modificar para crear un libro de cuentos igual a Calladium están indicadas por comentarios con esta estructura:

``` Tex
% ---------- Nombre de la sección ----------
Contenido
% --------------------
```
A continuación te indicamos qué elementos de la sección puedes cambiar para hacer tu propia versión de este libro.

### Portada

La portada contiene el título del libro, (una imagen o dos líneas) y el subtítulo.

Para editar esta información:

1. Busca la sección `% ---------- Portada ----------`.
2. Dirígete a la línea `{\fontsize{55}{0}\selectfont  Calladium} `. Sustituye `Calladium` por el nombre de tu libro. 
3. Busca la indicación `\noindent{\fontsize{28}{0}\selectfont Sueños Pandémicos}`. Sustituye `Sueños pandémicos` por el subtítulo de tu libro. Si tu libro no tiene subtítulo puedes colocar tu nombre en esta sección o borrar esta indicación.
4. Para cambiar la imagen por otra similar, dirígete a la sección [¿Cómo agrego imágenes?](/libro-de-suenos/agrego_imagenes/)

### Forros, Guarda 1 y Guarda 2

Para cambiar las imágenes en las guardas por las de tu preferencia sustituye las imágenes que se encuentran en las secciones `% ---------- Guarda 1 ----------` y `% ---------- Guarda 2 ----------`.

Para más información, dirígete a la sección [¿Cómo agrego imágenes?](/libro-de-suenos/agrego_imagenes/) 
 

### Cubierta

Para editar la información de la cubierta, busca la sección
 `% ---------- Cubierta ----------`.

Lo primero que puedes cambiar aquí es el título y el autor.

1. Para modificar el título, ve a:
``` Tex
{\fontsize{40}{0}\selectfont
Calladium}
```
Y sustituye Calladium por el nombre de tu libro. Por ejemplo:
``` Tex
\Huge
{\fontsize{40}{0}\selectfont
Sueñería}
```

2. Si quieres agregar un subtítulo, modifica `Sueños pandémicos` en el siguiente bloque de texto (si no te interesa un subtítulo borra este comando):

``` Tex
{\fontsize{28}{0}\selectfont
Sueños Pandémicos}\\
```

3. Para modificar el autor, sustituye `Libertad Pantoja` por tu nombre en el siguiente bloque de texto:

``` Tex
{\fontsize{28}{0}\selectfont
 Libertad Pantoja}
```

### Licencias

Para generar una página de licencias como la que se encuentra en Calladium edita la siguiente información:

1. En el siguiente bloque de texto sustituye `Calladium. Sueños pandémicos` por el nombre de tu libro.

``` TeX
\textsc{\textbf{Calladium. Sueños pandémicos}} % Nombre
\vskip\baselineskip
```
2. En la siguiente línea, sustituye `Primera versión, 2021` por la versión y el año de tu texto.

``` TeX
\noindent Primera versión, 2021
```

3. Para agregar la licencia que corresponde a tu texto, modifica  `2021, del texto e ilustraciones: Libertad Pantoja` en el siguiente código, en caso de que vaya a usar una licencia de copyright.

    ``` TeX
    \\
    \noindent  \copyright  2021, del texto e ilustraciones: Libertad Pantoja\\ % Copyright notice
    ```
    O bien, modifica `Licencia de la plantilla` en la siguiente línea para agregar una licencia de tipo Creative Commons. Para añadir una licencia Creative Commons diferente ve a ...
    ``` TeX
    Licencia de la plantilla \noindent Creative Commons \ccby \\
    ```
Puedes borrar cualquiera de las licencias que no necesites.

4. Para modificar a los participantes en la edición, sustituye `Edgar Ramírez Ruíz, Aaron Martínez Casillas y José Sánchez Sayago` en la siguiente línea.
``` TeX
\noindent \textsc{Edición:} \textit{Edgar Ramírez Ruíz, Aaron Martínez Casillas y José Sánchez Sayago}\\ 
```
5. Para modificar a los participantes en el diseño, sustituye `Carlo Fabrizzio Moncada` en la siguiente línea.
    ``` TeX
    \noindent \textsc{Diseño:} \textit{Carlo Fabrizzio Moncada}\\
    ```
    Puedes añadir secciones similares siguiendo la estructura `\noindent \textsc{Sección:} \textit{Participantes}\\`
6. Para editar el número de versión y cuándo fue elaborada sustituye `Primera versión elaborada en agosto de 2021` en la siguiente línea:
``` TeX   
\noindent Primera versión elaborada en agosto de 2021  % Printing/edition date 
```
7. En el siguiente espacio puedes sustituir `Calladium` por la Editorial o tu marca personal: 
    ``` TeX 
    \noindent \textsc{Calladium}\\
    ```
    Si incluyes la editorial, idealmente deberás añadir después la dirección de la misma. 
    Por ejemplo:
    ``` TeX 
    \noindent \textsc{Calladium}\\
    \noindent Paseo  de los gatos 33. Hacienda de Sotelo. \\
    \noindent Wonderland, Maine. \\\\
    ```
    En este caso los `\\` indican un salto de línea y los `\noindent` que no se quiere sangrar o identar el párrafo. Para saber más al respecto ve a la sección [¿Cómo cambio el diseño de mi libro? ](/libro-de-suenos/disenio_del_libro/)
8. Si quieres añadir un ISBN o lo estás tramitando puedes conservar y editar la siguiente línea. En caso contrario se sugiere que la borres.
    ``` TeX
    \noindent \textsc{ISBN: Pendiente de trámite}
    ```
9. Para añadir detalles del copyright y licencias que vas a ocupar modifica la información en el siguiente bloque:
``` TeX
\vskip\baselineskip
\noindent \small{
Se permite la reproducción total o parcial de este libro, así
como su transmisión sin fines de lucro por cualquier medio,
 respetando los derechos de autor con respecto al texto e ilustraciones.\\
La plantilla de Calladium se puede copiar y redistribuir en cualquier formato. Eres libre
de remezclar, transformar y construir a partir de esta obra para cualquier propósito, incluso uno comercial siempre y cuando des crédito a Calladium, el proyecto original.
\\
\\
```
10. Finalmente, sustituye `Web` por tu país o la región donde hayas elaborado tu libro en la siguiente línea:
``` TeX
\textbf{Hecho en la Web}
```

### Prólogo

Una vez que tengas listas las secciones anteriores, puedes modificar el prólogo. Para encontrar el prólogo, busca la etiqueta  `% ---------- Prólogo ----------`.

Sustituye el texto que encuentres aquí después del comando `\chapter{Prólogo}` para crear tu propia sección.

### Línea de tiempo

En caso de que no requieras la línea de tiempo puedes eliminar la sección completa `% ---------- Línea del tiempo ----------`. 

Si quieres usar esta sección. Ve a [¿Cómo creo una línea del tiempo?](/libro-de-suenos/experimentos_con_texto/linea_del_tiempo/)

### Contenido del libro de sueños

Para modificar el contenido del libro y añadir tus propios sueños o textos, busca el comentario `% ---------- Contenido del libro de sueños ----------`.

Si te interesa agrupar tus sueños con otro criterio que no sea los sucesos de la pandemia puedes modificar los nombres de los capítulos y agregar otros de acuerdo a tus vivencias. Por ejemplo:

``` Tex
\chapter{Infancia}
 
\subsection*{\hfill 10 de agosto 2000}
 
Lorem ipsum...
 
\chapter{Adolescencia}
 
\subsection*{\hfill 1 de enero 2008}
 
Lorem ipsum...
```
Donde `Pre-Pandemia` y `Pandemia` fueron sustituidos por `Infancia` y `Adolescencia`.

A partir de aquí, cada sueño está indicado por una fecha específica con la cual se crea una subsección del libro. El nombre de cada subsección va escrito entre corchetes después del comando `\hfill`. Por ejemplo:

``` Tex
\subsection*{\hfill 15 de abril 2019}

Tengo un jardín amplio en el techo del edificio lleno de helechos y plantas que debo cuidar.
```

Para cada sueño, modifica la fecha de la subsección y añade el texto que corresponde a tu sueño.

### Colofón (guarda 3)

Para editar el contenido del colofón sustituye el texto entre las etiquetas `\begin{colophon}` y `\end{colophon}` por tu propio colofón. Ejemplo:

``` Tex
\begin{colophon}
   Calladium. Sueños pandémicos.
\end{colophon}
```

### Guarda 4

Para cambiar la imagen en la guarda por la de tu preferencia sustituye la imagen que se encuentra en la sección `% ---------- Guarda 4 ----------`.

Para más información, dirígete a la sección [¿Cómo agrego imágenes?](/libro-de-suenos/agrego_imagenes/)

### Contraportada forros y contraportada

Para cambiar la imagen en la contraportada y en el forro de la contraportada  por la de tu preferencia sustituye las imágenes que se encuentra en las secciones `% ---------- Contraportada forros ----------` y `% ---------- Contraportada ----------`.

Para más información, dirígete a la sección [¿Cómo agrego imágenes?](/libro-de-suenos/agrego_imagenes/) 
