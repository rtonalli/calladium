+++
title = "¿Cómo hago mi propio libro de sueños?"
weight = 2
chapter = false
+++

Antes de empezar a crear tu propio libro de sueños vas a necesitar el ejemplo del cual partir. Puedes acceder al ejemplo de Calladium a través de un repositorio de GitHub o a través de una plantilla de Overleaf. Antes de empezar, crea un nuevo usuario en Overleaf.

## ¿Cómo obtengo el ejemplo aquí mismo?
Puedes obtener el ejemplo de Calladium aquí mismo. El archivo que te interesa es el que termina en .tex que es el que editarás y tomarás como base para tu libro de sueños.

{{%attachments style="gray" /%}}

1. Descarga todos los archivos.
2. Copia el contenido de `calladium.tex`.
3. Ingresa a Overleaf.
4. Da click en **Nuevo Proyecto**. 
5. Selecciona **Proyecto vacío**.
6. Sustituye el contenido de `main.tex` con el de `calladium.tex`.

## ¿Cómo obtengo el ejemplo desde GitHub?

Para descargar el ejemplo de [Calladium](https://github.com/LibertadPantoja-Wize/CalladiumSimple):

1. Ingresa a la carpeta `calladium_simple`.
2. Haz click en el archivo `calladium.tex`.
3. Descarga las imágenes que se encuentran en la carpeta `calladium_simple`.
4. Copia el contenido de `calladium.tex`.
5. Ingresa a Overleaf.
6. Da click en **Nuevo Proyecto**. 
7. Selecciona **Proyecto vacío**.
8. Sustituye el contenido de `main.tex` con el de `calladium.tex`.
9. Sigue el proceso en [¿Cómo agrego imágenes?](/libro-de-suenos/agrego_imagenes/) para añadir nuevas imágenes.

   
## ¿Cómo puedo utilizar la plantilla de Calladium?

1. Ingresa a Overleaf.
2. Da click en **Nuevo Proyecto**. Esto desplegará un nuevo menú. Ahí, en la sección de **Plantillas**, da click la opción **Ver Todas**.
3. En la barra de búsqueda escribe **calladium**.
4. Da click en **Search**.
5. Selecciona **Calladium simple**. Verás la siguiente pantalla:
![template](/Template.png)
6. Selecciona la opción **Open as Template**.


Tienes todo listo para empezar a construir tu libro de sueños basado en Calladium. Para encontrar un elemento o palabra en particular en Overleaf, lo puedes buscar tecleando `Ctrl + F` si tienes una PC (o algún sistema basado en Windows o Linux) o `Cmd + F` si tu computadora es Mac.

A partir de aquí puedes aprender los siguientes temas:

- [¿Cómo edito el texto?](/libro-de-suenos/edito_texto)
- [¿Cómo les doy formato a los párrafos?](/libro-de-suenos/edito_parrafos/)
- [¿Cómo agrego imágenes?](/libro-de-suenos/agrego_imagenes/)
- [¿Cómo añado una tabla?](/libro-de-suenos/agrego_una_tabla/)
- [¿Cómo agrego enlaces?](/libro-de-suenos/agrego_enlaces/)
- [¿Cómo creo referencias cruzadas y etiquetas?](/libro-de-suenos/referencias_cruzadas/)
- [Experimentos con texto](/libro-de-suenos/experimentos_con_texto/)
 
