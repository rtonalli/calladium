+++
title = "¿Cómo les doy formato a los párrafos?"
weight = 2
chapter = false
+++


Una vez que comiences a agregar tus sueños, notarás que a pesar de escribir el texto corrido, los párrafos no se separan de manera natural como en un procesador de textos. Esto sucede porque LaTeX necesita que le indiques que hay una nueva línea.

Para indicar un nuevo párrafo o espacio añade `\\` al final de un párrafo de la siguiente forma:

``` Tex
Voy a visitar a Atenea a Durango junto con unas amigas. Durango está a la orilla del mar.
Atenea es bióloga y nos pide que, antes de ir al evento,
la ayudemos a sacar los cadáveres que quedaron después del accidente del barco chino.
Caminamos por las calles de Durango.
\\
Durango tiene un cielo muy azul, muchos ríos y plantas. Las tiendas chinas son muy populares.
Nos detenemos un rato a comprar plumas de gel y a pasar al baño. 
Hemos caminado mucho y seguiremos caminando hasta el mar.
\\
Pasamos por un río lleno de insectos de colores,
bordeado de zacate amarillo y plantas acuáticas.
Atenea nos dice que lo malo es que casi todos los insectos
que se pueden ver son machos porque las hembras son muy pequeñas.
```

Una vez compilado, el texto se verá así:
![parrafo](/parrafo.png)

Añade tantos `\\` como saltos de línea necesites.

