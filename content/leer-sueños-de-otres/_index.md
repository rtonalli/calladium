+++
title = "Quiero leer sueños de otras personas"
weight = 5
chapter = false
+++

Existen muchos libros de sueños, algunos de ellos son:

- [Pero quién es el soñador](https://poesiamexa.files.wordpress.com/2018/12/Pero-qui%C3%A9n-es-el-so%C3%B1ador.-Sue%C3%B1os.pdf) de [Pierre Herrera](https://poesiamexa.wordpress.com/2018/12/27/pierre-herrera/).
- [Lengua noche](https://t.co/3xKXTSCnHq?amp=1) de [Rafael Villegas](https://www.goodreads.com/author/show/4407057.Rafael_Villegas).

Este es un sitio en construcción :) <i class="fas fa-tools"></i>

