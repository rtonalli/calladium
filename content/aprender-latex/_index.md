+++
title = "¿Qué son LaTeX y Overleaf?"
weight = 1
chapter = false
+++

## ¿Qué es Overleaf?
​
Overleaf es una aplicación web que te ayuda a crear documentos utilizando LaTeX y es la herramienta principal que usaremos para aprender en Calladium. Overleaf es completamente gratuito y no es necesario instalar nada en tu computadora.  
​  
Lo único que necesitas es [registrarte](https://www.overleaf.com/). Puedes hacerlo con tu correo personal, una cuenta de Google o con tu ORCID.  
​  
Overleaf te ayuda a crear un documento desde cero, y utilizando LaTeX, puedes generar el diseño completo y obtener un texto en PDF listo para publicar.  

![Registrate](/registro_overleaf.png)

## ¿Qué es LaTeX?

LaTeX es un sistema de preparación de documentos con el que puedes generar textos listos para imprimir y publicar. LaTeX te permite enfocarte en el contenido de tu texto y a través de sencillos comandos, LaTeX se encargará de dar el formato adecuado.  

LaTeX te permite concentrarte en el *qué* y no en el *cómo*.

Hay tres opciones para aprender LaTeX con Calladium solo usando Overleaf:

- Aprender solo lo necesario para hacer un libro de cuentos igual a Calladium.
- Aprender a usar cualquier plantilla de LaTeX.
- Aprender a hacer un documento de LaTeX desde cero para darle forma al proyecto.

Si esto no es lo que buscas, otras opciones para aprender LaTeX en español son:

- [LaTeX fácil](https://nokyotsu.com/latex/guia.html)
  
Este es un sitio en construcción :) <i class="fas fa-tools"></i>

