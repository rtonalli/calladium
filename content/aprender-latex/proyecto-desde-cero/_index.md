+++
title = "Quiero aprender a hacer un documento de LaTeX desde cero"
weight = 1
chapter = false
+++

## ¿Cómo puedo empezar un proyecto?

Al iniciar sesión, lo primero que ves es la página donde puedes administrar todos tus proyectos. Si la  interfaz está en inglés, realiza los siguientes pasos para cambiarla a español:

1. Da click sobre la bandera, en la parte inferior izquierda. 
2. Da clic en la bandera **Spanish** al desplegarse las opciones de idiomas. La página ahora se muestra en español. 

Para comenzar un proyecto nuevo, realiza los siguientes pasos:

1. Da click en **Nuevo Proyecto**, en la parte superior izquierda.
1. Da clic en la opción **Proyecto Vacío** al desplegarse el menú de opciones.
1. Introduce el nombre de tu proyecto y da clic en **Crear**. Al crear un proyecto verás la página que se muestra a continuación  

  ![Nuevo proyecto vista](/nuevo_proyecto_vista.png)

Ya que has comenzado un proyecto, vamos a revisar la interfaz sobre la que vamos a trabajar. Overleaf está dividido en tres diferentes columnas desde las que vas a poder crear tu texto. 

## ¿Cómo puedo gestionar los archivos de mi proyecto?

La primera columna es para administrar las secciones de tu proyecto. Overleaf te permite tener varios archivos en un mismo proyecto. Cuando creas un proyecto desde cero, el sistema genera automáticamente un archivo nuevo llamado `main.tex` que contiene las referencias básicas para que puedas trabajar utilizando LaTeX. 

Dentro de esta columna tienes tres diferentes opciones:

- **Archivo nuevo:** Agrega un nuevo archivo a tu proyecto. Crear un archivo te permite trabajar  capítulos o secciones independientes en tu texto.
- **Nueva carpeta**: Crea una carpeta en la que puedes agrupar los archivos de tu proyecto.
- **Subir**: Esta opción te permite agregar archivos que estén alojados en otros proyectos o en tu computadora. 

Adicionalmente, tienes la opción de **Renombrar** y **Eliminar** tanto archivos como carpetas dentro de tu proyecto. 

## ¿Cómo puedo comenzar a escribir con LaTeX?

Dentro de tu proyecto y ya con un archivo seleccionado, puedes comenzar tu texto utilizando LaTeX. Si has creado un proyecto desde cero, el sistema automáticamente genera una vista previa con algunos términos básicos de LaTeX. 

Esta columna es tu principal editor y es aquí donde se desarrolla el texto. Sigue el tutorial de LaTeX para poder comenzar a escribir. 

## ¿Cómo puedo exportar mi texto?

En la última columna del proyecto es donde Overleaf interpreta lo que escribes en LaTeX y genera la vista final. Conforme vayas escribiendo, esta columna te presenta el formato listo, en la parte superior encuentras el botón **Recompilar** con el que se actualiza lo que has generado y te lo muestra al instante. 

Finalmente tienes el ícono **Descargar PDF** con el que Overleaf genera un archivo en formato PDF listo para que lo puedas distribuir como tu prefieras, recuerda que el "hazlo tú mismo" es uno de los principales atractivos de Overleaf.  

## ¿Cómo puedo utilizar las plantillas de Overleaf?

Overleaf tiene una amplia gama de plantillas que puedes utilizar como base para tu nuevo texto. Para crear un proyecto desde una plantilla, realiza los siguientes pasos: 

1. Da clic en **Nuevo Proyecto** desde el menú principal. 
1. Selecciona la opción **Ver todas**, bajo la sección de **Plantillas**.
1. Elige la opción de la plantilla que mejor se adapte a tu proyecto.

## ¿Cómo funciona LaTeX?  

LaTeX es un lenguaje de marcado el cual al ser interpretado por algún sistema - Overleaf en nuestro caso - genera un texto completo con diferentes formatos. Una de las principales ventajas es que es sencillo de leer y aquí te proveeremos algunas herramientas para entender mejor.  

 LaTeX funciona a través de etiquetas que nos indican las características y propiedades del documento.  

Tomemos como ejemplo la información que aparece cuando generas un documento nuevo en Overleaf. El sistema automáticamente genera información que nos ayudará a comprender mejor la funcionalidad de LaTeX.

```latex

\documentclass{article}
\usepackage[utf8]{inputenc}

\title{Calladium}
\author{libertad.pantoja }
\date{July 2021}

\begin{document}

\maketitle

\section{Introduction}

\end{document}

```
## ¿Cómo funcionan las etiquetas?

Como podemos ver, las etiquetas se conforman de dos secciones principales. Primero definimos el parámetro que queremos modificar y después, entre llaves, el valor que debe de tomar.  

```\parametro{valor}```

## ¿Qué secciones conforman el documento?  
Ahora que ya sabemos lo básico de la funcionalidad de LaTeX, analicemos las partes principales que conforman un documento y que te ayudarán a dejar establecidos algunos parámetros.  

### Préambulo  

Primero tenemos el preámbulo del documento, el cual contiene que tipo de documento vamos a trabajar, en este caso con las etiquetas `\documentclass` y `\usepackage`. Aquí se define que clase de documento vamos a generar y si vamos a necesitar algún paquete adicional de tipografías.  

#### \documentclass  

Dentro de nuestra clase de documento podemos definir varios parámetros. Los parámetros se escriben dentro de corchetes antes de las llaves que definen la clase de documento y se separan con una coma sencilla. Principalmente se pueden utilizar para definir el tamaño de la fuente, el tamaño del papel y la orientación del mismo. Toma en cuenta el siguiente ejemplo:  

`\documentclass[12pt,a4paper,landscape]{book}`  

Primero tenemos la indicación del tamaño de letra. LaTeX soporta los tamaños 10p, 11pt y 12pt en este comando. Si quieres usar algún otro tamaño deberás establecerlo en cada fragmento de texto usando el comando `\fontsize`.
En segunda posición, tenemos el tamaño de papel el cual deseamos utilizar. Podemos definirlo con los siguientes parámetros:  

- **a4paper:** papel A4  
- **letterpaper:** tamaño carta  
- **a5paper:** papel A5  
- **b5paper:** papel B5  
- **Legalpaper:** tamaño legal  


Finalmente, tenemos la opción de cambiar la orientación del papel, en este caso utilizando el parámetro landscape.
Dentro de esta etiqueta, es necesario definir la clase del documento, puedes utilizar las siguientes opciones basado en la necesidad de tu texto:  
- **article:** para artículos en revistas científicas o reportes.  
- **minimal:** Sólo contiene tamaño de página y una fuente (tipo de letra) base. Es el tipo más simple.  
- **report:** Para reportes grandes con varios capítulos, puede servir para libros pequeños o tesis.  
- **book:** Para crear libros.  
- **slides:** Para crear presentaciones.  

#### \usepackage
Los paquetes te ayudan a mejorar la interfaz y a incrementar las funcionalidades de lo que puedes hacer con LaTeX, existen más de 4,000 diferentes tipos de paquetes y cada uno sugiere una implementación diferente. Si deseas explorar las opciones de paquetes y su funcionalidad, te invitamos a visitar el sitio [CTAN](https://www.ctan.org/pkg/) que contiene los paquetes y su funcionalidad. 
## Materia superior
Ya que hemos definido el formato con el que vamos a trabajar, es hora de trabajar nuestra obra. Esta es una de las partes más sencillas pues se puede deducir el contenido con solo leer las etiquetas. Qué son las siguientes:  
- \title  
- \author  
- \date    

En esta sección asignamos el título del texto, el autor y la fecha. Los valores de estos parámetros también se deben poner entre llaves. Si no le asignamos un valor a la fecha, Overleaf lo hará de manera automática, empatando la fecha en la que se comenzó el proyecto. 

### Cuerpo del documento
Ya que tenemos definido el comienzo de nuestro proyecto y lo hemos hecho de nuestra autoría, es necesario comenzar a trabajar en el cuerpo del texto. 
#### Principio y final
Existen dos etiquetas usadas de manera frecuente, con las cuales marcaremos el principio y el final de todo el documento o de una sección específica. 
Estas etiquetas las podemos encontrar como `\begin{}` y `\end{}`. Entre llave es necesario escribir el parámetro que queremos delimitar. Cualquier información contenida entre estas etiquetas formará el cuerpo de la sección y ayudará a Overleaf a definir cualquier cambio entre secciones.  
#### Secciones
Dependiendo de la longitud y el tipo de nuestro documento, es posible que queramos delimitar ciertas secciones como capítulos o dependencias dentro del mismo. El parámetro entre llaves indica el nombre asignado a la sección. Estas secciones las podemos delimitar con las siguientes etiquetas:  
- **`\part`:** Considerado como un nivel -1, se utiliza si tu documento tendrá diferentes partes.   
- **`\chapter`:** Considerado como un nivel 0, es el más común y se utiliza para separar los capítulos de tu documento.  
- **`\section`:** Considerado como un nivel 1, se utiliza para delimitar una sección dentro de un capítulo.  
- **`\subsection`:** Considerado como un nivel 2, se utiliza para delimitar una subsección dentro de la sección.  
- **`\paragraph`:** Considerado como un nivel 3, se utiliza para delimitar un párrafo dentro de una subsección.  
- **`\subparagraph`:** Considerado como un nivel 4, se utiliza para delimitar un subpárrafo dentro de un párrafo.  

#### Comentarios
En algunas ocasiones, es necesario realizar comentarios dentro del texto que no deben de ser publicados. Estos se utilizan para realizar anotaciones y explicar ciertas partes del documento. Ya sea sobre la intención del mismo o sobre la funcionalidad de algunas aplicaciones. Para realizar estos comentarios es necesario poner el símbolo % antes del comentario y LaTeX ignorará esa parte del texto, por lo cual no se compilará. 


Si quieres saber más de estos y otros tipos de documentos, visita:

- [What are the available “documentclass” types and their uses?](https://tex.stackexchange.com/questions/782/what-are-the-available-documentclass-types-and-their-uses)
- [LaTeX/Document Structure](https://en.wikibooks.org/wiki/LaTeX/Document_Structure#Document_classes)
- [Overleaf Documentation](https://www.overleaf.com/learn/latex/Creating_a_document_in_LaTeX)