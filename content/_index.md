+++
title = "Calladium"
chapter = false
+++

# Calladium <i class="fas fa-leaf"></i>

Esto es Calladium, un sitio web creado para que aprendas a usar [LaTeX](https://www.latex-project.org/) creando tu propio libro de sueños <i class="fas fa-book"></i> usando [Overleaf](https://es.overleaf.com/).

Las ventajas que te da el uso de LaTeX son:

- Crear archivos de texto compatibles con cualquier sistema operativo o plataforma. Estos archivos pueden exportarse como PDF.
- Escribir textos literarios, científicos, académicos o técnicos con una herramienta gratuita.
- Crear documentos grandes usando plantillas que puedes ajustar a tu gusto.
- Empezar a familiarizarte con las bases de programación.
  
Aunque aprender LaTeX involucra una curva de aprendizaje los beneficios son grandes:

- Enfocarte en escribir en lugar de usar tu tiempo para el diseño de la documentación al hacer uso de plantillas.
- Usar una herramienta profesional open-source con los mismos resultados que software propietario.
- Desarrollar nuevas habilidades que pueden llevarte a explorar otras áreas como la escritura técnica o la programación.

## ¿Qué te gustaría aprender?

- [¿Qué son LaTeX y Overleaf?](/aprender-latex/).
- [¿Cómo hago mi propio libro de sueños?](/libro-de-suenos/)
- [¿Qué son Git y Github?](/aprender-latex-y-tech/)
- [Quiero aprender por mi cuenta](/recursos-educativos/)
- [Quiero leer sueños de otras personas](/leer-sueños-de-otres/).

<div class="row">


<div class="container">
<div class="row">
<div class="col-md-3 col-lg-3 col-xs-3 col-sm-3">
<div class="card">
<div class="image"><img src="/overleaf.svg" width="100%" /></div>

<div class="text">

<h3>Overleaf</h3>

<p>Overleaf es una aplicación web que te ayuda a crear documentos utilizando LaTeX.</p>
<a href="/aprender-latex/">Ir a Overleaf</a>
</div>
</div>
</div>
</div>
</div>

<div class="container">
<div class="row">
<div class="col-md-3 col-lg-3 col-xs-3 col-sm-3">
<div class="card">
<div class="image"><img src="/latex.svg" width="100%" /></div>

<div class="text">

<h3>Libro</h3>

<p>LaTeX es un lenguaje markup para crear tu libro de sueños con una platilla.</p>
<a href="/libro-de-suenos/">Ir a LaTeX</a>
</div>
</div>
</div>
</div>
</div>

<div class="container">
<div class="row">
<div class="col-md-3 col-lg-3 col-xs-3 col-sm-3">
<div class="card">
<div class="image"><img src="/github-alt.svg" width="100%" /></div>

<div class="text">

<h3>Git</h3>

<p>Git es una herramienta de control de versiones y trabajo colaborativo.</p>
<a href="/aprender-latex-y-tech/">Ir a Git</a>

</div>
</div>
</div>
</div>
</div>

<div class="container">
<div class="row">
<div class="col-md-3 col-lg-3 col-xs-3 col-sm-3">
<div class="card">
<div class="image"><img src="/game.svg" width="100%" /></div>

<div class="text">

<h3>Recursos</h3>

<p>Recursos para jugar y aprender a usar las herramientas con las que constrirás tu libro.</p>
<a href="/recursos-educativos/">Ir a Recursos</a>

</div>
</div>
</div>
</div>
</div>

</div>


Este es un sitio en construcción :) <i class="fas fa-tools"></i>
