+++
title = "¿Qué son Git y GitHub?"
weight = 3
chapter = false
+++

Git es un Sistema de Control de Versiones (SCV). El control de versiones evita tener varias versiones separadas de un mismo archivo, como `documento_final.doc` y `documento_final_final.doc`, donde se pierde la noción de cuál es la versión más reciente y de los cambios hechos.

Git te permite:

-   Trabajar de forma local y remota

-   Regresar a versiones anteriores de un proyecto en cualquier momento

-   Llevar un control de quién ha hecho cambios y cuándo se hicieron

Una forma sencilla de trabajar con Git es usando GitHub. GitHub es una aplicación web que sirve para almacenar los proyectos de Git en internet y no solo de forma local. GitHub es completamente gratuito y puedes usarlo directamente desde tu navegador o instalarlo en tu computadora.

GitHub te permite:

-   Trabajar colaborativamente

-   Agregar un número ilimitado de colaboradores

-   Descargar y trabajar en tus proyectos desde cualquier dispositivo

-   Almacenar un número ilimitado de proyectos

## ¿Qué necesito saber para empezar a usar Git? 


Empezar a trabajar con Git puede ser un poco abrumador pero descuida porque la curva de aprendizaje no es tan grande como parece. Lo primero que deberás aprender son las definiciones de un vocabulario técnico, no es complicado pero sí necesario para avanzar en las siguientes secciones.

La siguiente tabla muestra el vocabulario técnico y sus definiciones:

<table>
  <tr>
    <th>Término</th>
    <th>Definición</th>
  </tr>
  <tr>
    <td><i>branch</i></td>
    <td>Una <i>branch</i> representa una línea independiente del desarrollo de tu proyecto. Es similar a las ramas de un árbol.</td>
  </tr>
  <tr>
    <td><i>repositorio</i></td>
    <td>Un <i>repositorio</i> es la carpeta donde estará guardado todo lo relacionado a tu proyecto. Aquí se registra el historial de los cambios.</td>
  </tr>
  <tr>
    <td><i>merge</i></td>
    <td><i>Merge</i> es una acción de Git en la que se junta el contenido de una *branch* con  otra. Usualmente, se hace merge entre las <i>branch de desarrollo</i> con la <i>branch principal</i> del proyecto.</td>
  </tr>
</table>

## ¿Cómo empiezo a usar Git/GitHub?

Lo primero que tendrás que hacer es registrarte en [GitHub](https://github.com/). Para registrarte necesitarás:

- Ingresar tu correo electrónico
- Crear una contraseña con mínimo de *15 caracteres* u *8 caracteres* incluyendo *1 o más números* y *1 letra en minúscula*
- Crear un nombre de usuario. GitHub te indicará si el nombre de usuario está disponible o no
- Responder si te gustaría recibir actualizaciones del producto o anuncios vía correo eléctronico. Debes responder con “y” para “*Sí”* o con “n” para “<i>No</i>”
- Resolver un pequeño acertijo para verificar que eres una persona. Tendrás que elegir a la galaxia en forma de espiral
- Dar click en el botón **Create Account**

![RegistroGitHub](/registro_github.png)

Al terminar de crear tu cuenta, tendrás que descargar la aplicación [GitHub Desktop](https://desktop.github.com/). Una vez descargada e instalada la aplicación, realiza los siguientes pasos:

1.  Inicia la aplicación de GitHub Desktop

2.  Inicia sesión con tu cuenta de usuario

3.  Selecciona la opción **Use my GitHub account name and email address**. El nombre y correo es para identificar los cambios que hagas.

4.  Da click en el botón **Finish**. 

Una vez completados los pasos anteriores, podrás empezar a crear repositorios en tu cuenta de GitHub y trabajar en ellos.

## ¿Cuáles son las funciones básicas de Git?

Las funciones básicas de Git son las siguientes:

-   Crear repositorios

-   Añadir archivos al repositorio

-   Subir cambios en los archivos

-   Descargar los últimos cambios hechos

-   Clonar un repositorio

### Crear un repositorio

Para crear un repositorio realiza las siguientes instrucciones:

1.  Inicia la aplicación de [GitHub Desktop](https://desktop.github.com/)

2.  Da click en el botón **+ Create a New Repository on your Hard Drive...** Una pequeña ventana aparecerá

3.  Ingresa un nombre para tu repositorio en el campo **Name**. Por ejemplo: `repo-tutorial`

4.  *Opcional*: Ingresa una descripción para tu repositorio en el campo de **Description**. Por ejemplo: *"Este es un repositorio de prueba para aprender a usar GitHub."*

5.  Da click en el botón **Choose...** para elegir la carpeta donde quieres guardar tu repositorio

6.  Selecciona la casilla **Initialize this repository with a README**. Esto creará un archivo `README.md` dentro del repositorio

7.  Selecciona la opción **None** en el campo **Git Ignore**

8.  Selecciona la opción **None** en el campo **License**

9.  Da click en el botón **Create Repository**

Al terminar tendrás listo tu repositorio de forma local, es decir, que solo existe en tu computadora. Para publicar el repositorio en tu cuenta de [GitHub](https://github.com/) realiza las siguientes instrucciones:

1.  Da click en el botón **Publish repository** que está en la parte superior de la aplicación. Esto te abrirá una pequeña ventana con un formulario

2.  Ingresa un nombre para tu repositorio de GitHub. Por defecto está el nombre que pusiste al crearlo, por lo que aconsejamos dejes el mismo nombre

3.  *Opcional*: Ingresa una descripción para tu repositorio en el campo de **Description**

4.  *Opcional*: Selecciona la casilla **Keep this code private** si quieres que tu repositorio sea privado para que sólo tú y tus colaboradores puedan encontrarlo

5.  Selecciona la opción **None** en el campo **Organization**

6.  Da click en el botón **Publish Repository**

Al terminar tu repositorio ya estará publicado y podrás verlo en tu cuenta de [GitHub](https://github.com/)

### Crear una branch

Una *branch* sirve para separar el desarrollo de tu proyecto en pequeñas partes. Los cambios que hagas en una *branch* no afectarán al resto del proyecto ni se verán reflejados hasta que se haga un *merge* con la *branch* principal del repositorio.

Para crear una branch en GitHub Desktop, realiza las siguientes instrucciones:

1.  Da click en el botón **Current Branch main** que está en la parte superior de la aplicación. Se mostrará una lista con todas las branch existentes en el repositorio

2.  Da click en el botón **New Branch**. Se abrirá una pequeña ventana.

3.  Ingresa un nombre para tu nueva *branch*

4.  Da click en el botón **Create Branch**

Al terminar habrás creado una branch de forma local. Para publicar la branch en tu repositorio de GitHub basta con que des click en botón **Publish branch**.

### Crear y publicar cambios

Dentro del repositorio que creaste en tu computadora hay un archivo `README.md` que puedes editar. Si no lo hay, puedes agregar cualquier archivo de texto que puedas editar a tu repositorio. Cuando tengas el archivo que quieres editar dentro de tu repositorio, realiza las siguientes instrucciones:

1.  Abre el archivo que quieres editar. Por ejemplo el archivo `README.md`

2.  Agregar cualquier cambio que quieras a tu archivo. Por ejemplo, puedes agregar tu nombre al inicio

3.  Guarda los cambios

Al terminar los pasos anteriores, dentro de la aplicación de GitHub Desktop podrás ver los cambios realizados a tu archivo. Para subir los cambios a tu branch realiza las siguientes instrucciones:

1.  Escribe un resumen de los cambios realizados en el campo **Summary (required)** que se encuentra en la parte inferior izquierda de la aplicación. Por ejemplo: *"Nombre del autor agregado"*

2.  *Opcional*: Escribe una descripción más detallada de los cambios en el campo **Description**. Por ejemplo: *"Se agregó el nombre del autor al inicio del archivo para mayor visualización"*

3.  Da click en el botón **Commit to [nombre de tu branch]**. Al hacer esto estarás agregando los cambios a tu *branch* de forma local

4.  Da click en el botón *Push origin* para subir tus cambios al repositorio de GitHub

Al terminar, los cambios realizados en tu archivo estarán disponibles en tu cuenta de GitHub.

## ¿Cómo puedo revertir los cambios hechos?

Es muy probable que en cualquier momento quieras revertir cambios que hayas subido. GitHub Desktop te permite regresar una versión anterior a la vez. Es decir, si tu proyecto ya lleva 3 versiones y quieres regresar a la primera versión, tendrás que empezar por regresar a la segunda versión y solo entonces podrás regresar a la primera versión.

Para revertir los cambios hechos, realiza las siguientes instrucciones:

1.  Da click en la pestaña **History** que está en la parte superior izquierda de la aplicación. Aquí se mostrará el historial de cambios que se han subido al repositorio.

2.  Da click derecho en el primer cambio que aparece en el historial de arriba hacia abajo. Aparecerá una lista de opciones

3.  Da click en la opción **Revert Changes in Commit**. Esto revertirá todos los cambios hechos en esa versión.

Al terminar, tu *branch* habrá revertido los cambios hechos de forma local. Si también quieres que se reviertan los cambios en GitHub tendrás que volver a subir la versión como si fuera la primera vez que lo haces.

## ¿Cómo puedo invitar a colaboradores a mi proyecto?

Para poder invitar a otras personas a colaborar en tu proyecto todas deberán tener una cuenta de GitHub. Si tus colaboradores ya cuentan con una cuenta de GitHub, realiza las siguientes instrucciones para invitarlos a tu proyecto:

1.  Ve a tu repositorio en GitHub

2.  Da click en la pestaña **Settings**. Se mostrará un menú de opciones a la izquierda.

3.  Da click en la opción **Manage Access**

4.  Da click en el botón **Invite a collaborator**. Se abrirá una pequeña ventana

5.  Ingresa el nombre de usuario, nombre completo, o correo electrónico de la persona que quieres invitar. Aparecerá una lista de sugerencias

6.  Da click en el nombre de la persona que quieres invitar como colaborador

7.  Da click en el botón **Add [usuario] to this repository**. En la parte de abajo de la pantalla podrás ver una lista de las personas que has invitado

Cuando acepten la invitación, tus colaboradores podrán empezar a crear sus propias *branch* y subir cambios a tu repositorio.

## ¿Cómo puedo usar Overleaf con GitHub?

Una de las mayores ventajas de [GitHub](https://github.com/) es su fácil integración con otras tecnologías. Por ejemplo, puedes crear y editar un proyecto en tu cuenta de [Overleaf](https://overleaf.com/) y conectarla con tu cuenta de GitHub para crear tu repositorio y llevar el control de versiones.

### Conectar Overleaf con GitHub

Para conectar tu cuenta de [Overleaf](https://www.overleaf.com/) con tu cuenta de [GitHub](https://github.com/):

1.  Inicia sesión en tu cuenta de Overleaf

2.  Da click en el botón **Cuenta** que está en la parte superior derecha. Se desplegará una lista de opciones

3.  Da click en la opción **Opciones de la cuenta**

4.  Da click en el botón **Enlace a tu cuenta de GitHub** en la sección **Integración con GitHub**. Se abrirá una nueva ventana

5.  Ingresa tu correo electrónico o nombre de usuario, y contraseña de GitHub para autorizar la conexión entre Overleaf y GitHub

Al terminar tu cuenta de Overleaf estará conectada con GitHub.

### Crear un proyecto de Overleaf con un repositorio de GitHub

La vinculación de Overleaf y GitHub te permitirá crear repositorios y subir los cambios directamente desde tu cuenta de Overleaf. 

Para crear un repositorio desde un proyecto de Overleaf:

1.  Abre tu proyecto de Overleaf

2.  Da click en el botón **Menú** que está en la parte superior izquierda. Se desplegará un menú lateral

3.  Da click en la opción **GitHub**. Se mostrará un pequeño formulario titulado **Sincronización con GitHub**

    ![SyncOverleafGithub](/sincronizar_overleaf_github.png)

4.  Ingresa un nombre para el repositorio en el campo **Nombre del repositorio**

5.  *Opcional*: Ingresa una descripción del repositorio en el campo **Descripción**

6.  Selecciona una de las casillas **Público** o **Privado**

7.  Da click en el botón **Crear un repositorio en GitHub**

Al terminar, tu repositorio estará listo y podrás verlo en tu cuenta de GitHub.

### Subir cambios de Overleaf a GitHub

Para subir los cambios a GitHub directamente desde tu proyecto en Overleaf:

1.  Abre tu proyecto de Overleaf

2.  Da click en el botón **Menú** que está en la parte superior izquierda. Se desplegará un menú lateral

3.  Da click en la opción **GitHub**. Se mostrará un pequeña ventana

4.  Da click en el botón **Envía cambios de Overleaf a GitHub**

5.  Opcional:  Ingresa una descripción de los cambios realizados

6.  Da click en el botón **Commit**

Al terminar, podrás ver los cambios realizados en tu repositorio de GitHub.
